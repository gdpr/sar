<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle. If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the polyfill to allow a plugin to operate with Moodle
 * 3.3 up.
 *
 * @package core_privacy
 * @copyright 2018 Andrew Nicols <andrew@nicols.co.uk>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace core_privacy;

use \core_privacy\metadata\item_collection;
use \core_privacy\request\approved_contextlist;
use \core_privacy\request\deletion_criteria;

trait legacy_polyfill {

    /**
     * Get the language string identifier with the component's language
     * file to explain why this plugin stores no data.
     *
     * @return  String
     */
    public static function get_reason() : String {
        return static::_get_reason();
    }

    /**
     * @return array The array of metadata
     */
    public static function get_metadata(item_collection $itemcollection) : item_collection  {
        return static::_get_metadata($itemcollection);
    }

    /**
     * Export all user preferences for the plugin.
     *
     * @param   int         $userid The userid of the user whose data is to be exported.
     */
    public static function export_user_preferences(int $userid) {
        return static::_export_user_preferences($userid);
    }

    /**
     * Get the list of contexts that contain user information for the specified user.
     *
     * @param   int         $userid     The user to search.
     * @return  contextlist   $contextlist  The contextlist containing the list of contexts used in this plugin.
     */
    public static function get_contexts_for_userid(int $userid) : contextlist {
        return static::_get_contexts_for_userid($userid);
    }

    /**
     * Export all user data for the specified user, in the specified contexts.
     *
     * @param   approved_contextlist    $contextlist    The approved contexts to export information for.
     */
    public static function export_user_data(approved_contextlist $contextlist) {
        return static::_export_user_data($contextlist);
    }

    /**
     * Delete all use data which matches the specified deletion_criteria.
     *
     * @param   deletion_criteria       $criteria   An object containing specific deletion criteria to delete for.
     */
    public static function delete_for_context(deletion_criteria $criteria) {
        return static::_delete_for_context($criteria);
    }

    /**
     * Delete all user data for the specified user, in the specified contexts.
     *
     * @param   approved_contextlist    $contextlist    The approved contexts and user information to delete information for.
     */
    public static function delete_user_data(approved_contextlist $contextlist) {
        return static::_delete_user_data($contextlist);
    }
}
